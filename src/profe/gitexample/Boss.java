package profe.gitexample;

public class Boss {

	private Worker1 worker1 = new Worker1();
	
	public static void main(String[] args) {
		new Boss().go();
	}

	private void go() {
		System.out.println(worker1.getWork());
	}
}
